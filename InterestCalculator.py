#!/usr/bin/python3

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ui_interest import Ui_InterestCalculator

__version__='1.0.0'

class InterestCalculator(QDialog,
    Ui_InterestCalculator):
        
    def __init__(self,parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.principalSpinBox.setFocus()
    
    @pyqtSignature("int")
    def on_typeComboBox_currentIndexChanged(self):
        self.updateVal()

    @pyqtSignature("double")
    def on_principalSpinBox_valueChanged(self):
        self.updateVal()

    @pyqtSignature("double")
    def on_rateSpinBox_valueChanged(self):
        self.updateVal()

    @pyqtSignature("double")
    def on_yearSpinBox_valueChanged(self):
        self.updateVal()

    def updateVal(self):
        try:
            principal=self.principalSpinBox.value()
            rate=self.rateSpinBox.value()
            year=self.yearSpinBox.value()
        except TypeError:
            message="{} must be a float value, not {}"
            if not type(principal,float):
                message=message.format('Principal',principal)
                self.principalSpinBox.setFocus()
                self.principalSpinBox.selectAll()
            if not type(rate,float):
                message=message.format('Rate',rate)
                self.rateSpinBox.setFocus()
                self.rateSpinBox.selectAll()
            if not type(year,float):
                message=message.format('Year',year)
                self.yearSpinBox.setFocus()
                self.yearSpinBox.selectAll()
            amount='undefined'
            QMessageBox.warning(self,'Value Error', message)
        else:
            x=self.typeComboBox.currentIndex()
            if x==0:
                amount=principal+((principal*(rate/100.0))*year)
            elif x==1:
                amount=principal*((1+(rate/100.0))**year)
        finally:
            self.amountSpinBox.setValue(amount)


if __name__=='__main__':
    import sys
    qapp=QApplication(sys.argv)
    form=InterestCalculator()
    form.show()
    qapp.exec_()


            

    




