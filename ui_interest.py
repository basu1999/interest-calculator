# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interest.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_InterestCalculator(object):
    def setupUi(self, InterestCalculator):
        InterestCalculator.setObjectName(_fromUtf8("InterestCalculator"))
        InterestCalculator.resize(281, 295)
        self.widget = QtGui.QWidget(InterestCalculator)
        self.widget.setGeometry(QtCore.QRect(0, 10, 275, 327))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.gridLayout = QtGui.QGridLayout(self.widget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.widget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.typeComboBox = QtGui.QComboBox(self.widget)
        self.typeComboBox.setInputMethodHints(QtCore.Qt.ImhNone)
        self.typeComboBox.setObjectName(_fromUtf8("typeComboBox"))
        self.typeComboBox.addItem(_fromUtf8(""))
        self.typeComboBox.addItem(_fromUtf8(""))
        self.horizontalLayout.addWidget(self.typeComboBox)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 2)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 1, 0, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(20, 18, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 1, 1, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_2 = QtGui.QLabel(self.widget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_2)
        self.principalSpinBox = QtGui.QDoubleSpinBox(self.widget)
        self.principalSpinBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.principalSpinBox.setButtonSymbols(QtGui.QAbstractSpinBox.UpDownArrows)
        self.principalSpinBox.setDecimals(4)
        self.principalSpinBox.setMaximum(1000000000.0)
        self.principalSpinBox.setObjectName(_fromUtf8("principalSpinBox"))
        self.horizontalLayout_2.addWidget(self.principalSpinBox)
        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 0, 1, 2)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_3 = QtGui.QLabel(self.widget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_3.addWidget(self.label_3)
        self.rateSpinBox = QtGui.QDoubleSpinBox(self.widget)
        self.rateSpinBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.rateSpinBox.setDecimals(4)
        self.rateSpinBox.setMaximum(100.0)
        self.rateSpinBox.setObjectName(_fromUtf8("rateSpinBox"))
        self.horizontalLayout_3.addWidget(self.rateSpinBox)
        self.gridLayout.addLayout(self.horizontalLayout_3, 3, 0, 1, 2)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 3, 2, 1, 1)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_4 = QtGui.QLabel(self.widget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_4.addWidget(self.label_4)
        self.yearSpinBox = QtGui.QDoubleSpinBox(self.widget)
        self.yearSpinBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.yearSpinBox.setMaximum(1000000.0)
        self.yearSpinBox.setObjectName(_fromUtf8("yearSpinBox"))
        self.horizontalLayout_4.addWidget(self.yearSpinBox)
        self.gridLayout.addLayout(self.horizontalLayout_4, 4, 0, 1, 2)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem3, 5, 0, 1, 1)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.label_5 = QtGui.QLabel(self.widget)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.horizontalLayout_5.addWidget(self.label_5)
        self.amountSpinBox = QtGui.QDoubleSpinBox(self.widget)
        self.amountSpinBox.setReadOnly(True)
        self.amountSpinBox.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.amountSpinBox.setDecimals(5)
        self.amountSpinBox.setMaximum(1000000000.0)
        self.amountSpinBox.setObjectName(_fromUtf8("amountSpinBox"))
        self.horizontalLayout_5.addWidget(self.amountSpinBox)
        self.gridLayout.addLayout(self.horizontalLayout_5, 6, 0, 1, 2)
        spacerItem4 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem4, 7, 0, 1, 1)
        self.label.setBuddy(self.typeComboBox)
        self.label_2.setBuddy(self.principalSpinBox)
        self.label_3.setBuddy(self.rateSpinBox)
        self.label_4.setBuddy(self.yearSpinBox)

        self.retranslateUi(InterestCalculator)
        QtCore.QMetaObject.connectSlotsByName(InterestCalculator)

    def retranslateUi(self, InterestCalculator):
        InterestCalculator.setWindowTitle(_translate("InterestCalculator", "Interest Calculator", None))
        self.label.setText(_translate("InterestCalculator", "&Interest Type:", None))
        self.typeComboBox.setItemText(0, _translate("InterestCalculator", "Simple", None))
        self.typeComboBox.setItemText(1, _translate("InterestCalculator", "Compound", None))
        self.label_2.setText(_translate("InterestCalculator", "&Principal:", None))
        self.principalSpinBox.setPrefix(_translate("InterestCalculator", "$", None))
        self.label_3.setText(_translate("InterestCalculator", "&Rate:", None))
        self.rateSpinBox.setSuffix(_translate("InterestCalculator", "%", None))
        self.label_4.setText(_translate("InterestCalculator", "&Years:", None))
        self.yearSpinBox.setSuffix(_translate("InterestCalculator", " years", None))
        self.label_5.setText(_translate("InterestCalculator", "Amount:", None))
        self.amountSpinBox.setPrefix(_translate("InterestCalculator", "$", None))

